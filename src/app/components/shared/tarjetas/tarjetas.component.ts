import { Component, Input, OnInit } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.component.html',
  styleUrls: ['./tarjetas.component.css']
})
export class TarjetasComponent implements OnInit {

  @Input() items:any [] = [];

  constructor(private _service:SpotifyService,private r:Router) { }

  ngOnInit(): void {
  }

  verArtista(item:any){
    //console.log(item)
    let artistaID

    if (item.type === 'artist') {
      artistaID = item.id
    }else {
      artistaID = item.artists[0].id
    }

    this.r.navigate(['/artist',artistaID])

    // this._service.findById(artistaID).subscribe((data)=>{
    //   console.log(data)
    // });

  }

}
