import { Component, OnInit } from '@angular/core';


import { SpotifyService } from "../../services/spotify.service";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  artistas : any [] = []
  loading: boolean;

  constructor(private _service:SpotifyService) {
  }

  ngOnInit(): void {
  }

  buscar(termino:string){

    if (termino.length > 0) {

      this.loading = true
    }
    console.log(termino)
    //buscar

    this._service.buscar(termino).subscribe((data : any) => {
      console.log(data)
     this.artistas=data
     this.loading = false
   });

   console.log(this.artistas)
  }

}
