import { Component, OnInit } from '@angular/core';

//servicios
import { SpotifyService } from "../../services/spotify.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // paises: any[] = [];
  nuevasCanciones: any[] = [];
  loading: boolean;
  error: boolean = false;
  mensajeError:string;

  constructor(private _service:SpotifyService) {
    // this.http.get('https://restcountries.eu/rest/v2/lang/es').subscribe( (data:any) => {
    //   console.log(data)
    //   this.paises = data;
    // });

    this.loading = true

    this._service.getNewRelease().subscribe((data : any) => {
       console.log(data)
      this.nuevasCanciones=data
      this.loading = false
    }, (err) => {
      console.log(err)
      this.error = true;
      this.loading = false

      this.mensajeError = err.error.error.message
    });
  }

  ngOnInit(): void {
  }

}
