import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styleUrls: ['./artista.component.css']
})
export class ArtistaComponent implements OnInit {

  artista:any = {};
  top:any = [] = [];
  loading:boolean = true;

  constructor(private _service:SpotifyService,private route:ActivatedRoute) {

    this.route.params.subscribe(params => {

      this.getTracks(params['id']);
      this.getArtista(params['id']);
    })


   }

  ngOnInit(): void {
  }

  getArtista(id:string){

    this._service.findById(id).subscribe((data)=>{
      console.log(data)
      this.artista = data;

      this.loading = false;
    });

  }
  getTracks(id:string){

    this._service.toptrack(id).subscribe((data)=>{
      console.log(data)
      this.top = data;
    });

  }

}
