import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  URL:string = 'https://api.spotify.com/v1';

  constructor(private http: HttpClient) {
    console.log('servicio')
  }

  getNewRelease() {

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQArQ2DoRN9qCd0M1DFixt0mr6b_Yn72Gn0_z6ajtc-9xHZ8sU8ZpLRollmAW7tqyNvmircwACIfIhN3PcE'
    });

    return this.http.get(`${this.URL}/browse/new-releases`, { headers })
      .pipe(map(data => {
        return data['albums'].items
      }))
  }

  buscar(termino: string) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQArQ2DoRN9qCd0M1DFixt0mr6b_Yn72Gn0_z6ajtc-9xHZ8sU8ZpLRollmAW7tqyNvmircwACIfIhN3PcE'
    });

    return this.http.get(`${this.URL}/search?q=${termino}&type=artist&limit=15`, { headers }).pipe(map(data => {
      return data['artists'].items
    }));
  }
  findById(id:string){
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQArQ2DoRN9qCd0M1DFixt0mr6b_Yn72Gn0_z6ajtc-9xHZ8sU8ZpLRollmAW7tqyNvmircwACIfIhN3PcE'
    });
    // v1/artists/{id}
    return this.http.get(`${this.URL}/artists/${id}`, { headers }).pipe(map(data => {
      return data
    }));
  }
  toptrack(id:string){
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQArQ2DoRN9qCd0M1DFixt0mr6b_Yn72Gn0_z6ajtc-9xHZ8sU8ZpLRollmAW7tqyNvmircwACIfIhN3PcE'
    });
    // v1/artists/{id}
    return this.http.get(`${this.URL}/artists/${id}/top-tracks?country=us`, { headers }).pipe(map(data => {
      return data['tracks'];
    }));
  }
}
